'use strict';

import {checkIfLearnersConfigFileExist, checkIfDatasDirExistsInActivity, chooseActivity, chooseDifficulty, enterRessourceId, loadRessourceObjectFromFile} from './main_h.js';

const main = () => {

    async function run()
    {
        checkIfLearnersConfigFileExist('./learners.yml');

        // Étant donné que javascript ne force pas le typage des variables
        // J'ai pris l'habitude pour ce type de langage de commencer le nom de mes variables par une première lettre indiquant le type de données (au sens large) que je manipule 
        
        const sActivity     = await chooseActivity(); // sActivity commence par un s, donc => string
        const mActivity     = await import('./activities/' + sActivity + '/activity.js'); // m => module, autrement dit la variable 'mActivity' contient les informations relatives au fichier .js importé via la fonction 'import(...)'

        let oRessource;
        if(checkIfDatasDirExistsInActivity(sActivity) === true){
            // certaines activités n'utilisent pas de dossier datas, tel que l'activité "onlyChoice"
            const sDifficulty   = await chooseDifficulty(sActivity); // s => string
            const ressourceId   = await enterRessourceId(sActivity, sDifficulty); // pour ressourceId : un id est la plupart du temps un integer
            oRessource          = await loadRessourceObjectFromFile(sActivity, sDifficulty, ressourceId); // oRessource : o => objet
        }

        await mActivity.run(oRessource);
    }

    return run();
};

main();
