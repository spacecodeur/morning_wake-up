'use strict';

import {askChoice, input, error} from './libraries/terminal.js';
import {isFileExist, isDirExists, getAvailablesActivities, getAvailablesDifficulties, yamlFileToJavascriptObject} from './libraries/fileSystem.js';

async function checkIfLearnersConfigFileExist(sLearnersFile)
{
    if(! isFileExist(sLearnersFile))
    {
        throw new Error('The learners config file was not found. You must copy the ./datas/learners.yml.dist to ./datas/learners.yml and fill it with your learners firstnames.');
    }
}

function checkIfDatasDirExistsInActivity(sActivity)
{
    return isDirExists('./activities/' + sActivity + '/datas/');
}

async function chooseActivity()
{
    return await askChoice('Choose activity type :', 
        ...getAvailablesActivities().map((activity)=>{
            return {value:activity};
        }
    ));
}

async function chooseDifficulty(sActivity)
{
    return await askChoice('Choose the difficulty :', 
        ...getAvailablesDifficulties(sActivity).map((difficulty)=>{
            return {value:difficulty};
        }
    ));
}

function _getRessourcePath(sActivity, sDifficulty, ressourceId)
{
    return './activities/' + sActivity + '/datas/' + sDifficulty + '/' + ressourceId + '.yml';
}

function loadRessourceObjectFromFile(sActivity, sDifficulty, ressourceId){
    return yamlFileToJavascriptObject(_getRessourcePath(sActivity, sDifficulty, ressourceId));
}

async function enterRessourceId(sActivity, sDifficulty)
{
    let ressourceId;
    let isRessourceIdValid;

    do{
        if(isRessourceIdValid === true)
        {
            console.log(error('[' + sActivity + '>' + sDifficulty + '] ressource ' + ressourceId + ' not found !'));
            isRessourceIdValid = false;
        }
        else
        {
            isRessourceIdValid = false;
        }
        
        do{
            ressourceId = await input({ message : 'Enter the id of the ressource : '});
        }while(! /^\+?(0|[1-9]\d*)$/.test(ressourceId)); // l'id doit être un nombre
        
        
        if(! isFileExist(_getRessourcePath(sActivity, sDifficulty, ressourceId))){
            isRessourceIdValid = true;
        }

    }while(isRessourceIdValid === true);

    return ressourceId;
}

export
{
    checkIfLearnersConfigFileExist,
    checkIfDatasDirExistsInActivity,
    loadRessourceObjectFromFile,
    chooseActivity,
    chooseDifficulty,
    enterRessourceId
};