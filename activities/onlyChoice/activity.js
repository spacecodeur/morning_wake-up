"use strict";

import {
  askChoiceYesNo,
  importantText,
  askChoice,
  askUnchoiceMultiple,
  error,
  input,
  pressEnterFor
} from "../../libraries/terminal.js";
import {
  pickRandomLearner,
  loadLearnersObjectsFromFile,
  incrementScoreInLearnerConfigFile,
} from "../../libraries/learners.js";

const l = console.log;
const lc = console.clear;

async function run(oAlgos) {
  lc();

  let oLearners = loadLearnersObjectsFromFile();

  // On demande quels sont les apprenants présents
  let aLearnersPresents;
  do {
    aLearnersPresents = await askUnchoiceMultiple(
      "Tout le monde est bien présent ? (décocher les absents)",
      ...Object.keys(oLearners).map((name) => ({ value: name }))
    );
    if (aLearnersPresents.length === 0)
      l(error("Hmmm, il faut qu'il y ai au moins un présent !"));
  } while (aLearnersPresents.length === 0);

  do {
    let answer = await askChoice(
      "How is the chosen one ?",
      { name: "Randomly way", value: "random" },
      { name: "manually way", value: "manually" },
      { name: "exit", value: "exit" }
    );

    if(answer === "exit"){
      break;
    }

    if (answer === "random") {
      let validated = false;
      let theChoosenOne;

      oLearners = await removeMissingsLearners(aLearnersPresents);

      do {
        theChoosenOne = pickRandomLearner(oLearners, "onlyChoice");
        l();
        l(importantText(theChoosenOne) + " ! are you ready ?!");
        l();

        validated = await askChoiceYesNo("confirm : ");
      } while (validated === "no");

      // on ajoute +1 dans le learners.yml à l'apprenant choisi
      // il aura moins de chance d'être choisi la fois prochaine
      incrementScoreInLearnerConfigFile(oLearners, theChoosenOne, "onlyChoice");
    
      lc();
      await pressEnterFor("Do the thing!");
      l();
    }
  } while (true);

  await input({ message: "bye ! have a good day :D" });
}

async function removeMissingsLearners(aLearnersPresents) {
  // on reprend les datas dans le fichier .yml à chaque nouvelle question
  // pour avoir les dernières infos à jours pour l'algo ramdom
  let oLearners = await loadLearnersObjectsFromFile();

  // On vire les apprenants absents de la structure principale
  return Object.keys(oLearners)
    .filter((key) => aLearnersPresents.includes(key))
    .reduce((obj, key) => {
      obj[key] = oLearners[key];
      return obj;
    }, {});
}

export { run };