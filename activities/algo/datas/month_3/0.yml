-
  statement: |-
    0. Écrire un programme ou un algorithme qui permet d'afficher tous les nombres allant de 0 à 100
    1. Créer une fonction qui, en fonction d'un nombre n donné, affiche tous les nombres allant de 0 à n
    2. Améliorer la fonction: elle ne doit plus rien afficher mais elle doit retourner la somme de tous les entiers compris entre 0 et n
    3. Modifier la fonction: elle doit retourner la somme de tous les entiers multiples de 3 compris entre 0 et n 
    4. Je veux additionner x fois tous les nombres multiples de 3 compris entre 0 et n : que dois-je modifier/ajouter dans mon programme ?
  help:
  answers: 
    pseudocode: |-
      // 0
      POUR i allant de 0 à 100
        AFFICHER i
      FIN POUR

      // 1
      FONCTION afficherNombres(n: entier)
        POUR i allant de 0 à entier
          AFFICHER i
        FIN POUR
      FIN FONCTION
    
      // 2
      FONCTION sommeNombres(n: entier)
        somme <- 0
        POUR i allant de 0 à entier
          somme <- somme + i
        FIN POUR
        RETOURNER somme
      FIN FONCTION

      // 3 
      FONCTION sommeMultiplesDeTrois(n: entier)
        somme <- 0
        POUR i allant de 0 à entier avec un pas de 3
          somme <- somme + i
        FIN POUR
        RETOURNER somme
      FIN FONCTION

      // 4
      FONCTION sommeMultiplesDeTroisXfois(n: entier, x: entier)
        somme <- 0
        POUR i allant de 0 à entier avec un pas de 3
          somme <- somme + i
        FIN POUR
        RETOURNER somme * x
      FIN FONCTION

    javascript: |-
      // 0
      for (let i = 0; i <= 100; i++) {
        console.log(i);
      }

      // 1 
      function afficherNombres(n) {
        for (let i = 0; i <= n; i++) {
            console.log(i);
        }
      }

      // 2
      function sommeNombres(n) {
        let somme = 0;
        for (let i = 0; i <= n; i++) {
            somme += i;
        }
        return somme;
      }

      // 3
      function sommeMultiplesDeTrois(n) {
        let somme = 0;
        for (let i = 0; i <= n; i += 3) {
            somme += i;
        }
        return somme;
      }

      // 4
      function sommeMultiplesDeTroisXfois(n, x) {
        let somme = 0;
        for (let i = 0; i <= n; i += 3) {
            somme += i;
        }
        return somme * x;
      }

    php: |-
      // 0
      for ($i = 0; $i <= 100; $i++) {
          echo $i . "\n";
      }

      // 1
      function afficherNombres($n) {
          for ($i = 0; $i <= $n; $i++) {
              echo $i . "\n";
          }
      }

      // 2
      function sommeNombres($n) {
          $somme = 0;
          for ($i = 0; $i <= $n; $i++) {
              $somme += $i;
          }
          return $somme;
      }

      // 3
      function sommeMultiplesDeTrois($n) {
          $somme = 0;
          for ($i = 0; $i <= $n; $i += 3) {
              $somme += $i;
          }
          return $somme;
      }

      // 4
      function sommeMultiplesDeTroisXfois($n, $x) {
          $somme = 0;
          for ($i = 0; $i <= $n; $i += 3) {
              $somme += $i;
          }
          return $somme * $x;
      }
    
    python: |-
      # 0
      for i in range(101):
          print(i)

      # 1
      def afficher_nombres(n):
          for i in range(n + 1):
              print(i)

      # 2
      def somme_nombres(n):
          somme = 0
          for i in range(n + 1):
              somme += i
          return somme

      # 3
      def somme_multiples_de_trois(n):
          somme = 0
          for i in range(0, n + 1, 3):
              somme += i
          return somme

      # 4
      def somme_multiples_de_trois_x_fois(n, x):
          somme = 0
          for i in range(0, n + 1, 3):
              somme += i
          return somme * x

-
  statement: |-
    // En programmation, une fonction qui s'appelle elle-même est une fonction dîtes 'récursive'. La récursivité permet de construire des algorithmes plus élégants et parfois même plus performants que des approches plus classiques.

    FONCTION mystere(t: tableau)
      SI(TAILLE(t) est égal à 0)
        RETOURNER 0
      SINON
        RETOURNER t[0] + mystere(t[1:])
      FIN SI
    FIN FONCTION

    // 0. Quel résultat affiche la ligne suivante ? 
    AFFICHER(mystere([4,9,0,5]))

    // 1. Améliorer la fonction mystere afin que le calcul qu'elle effectue ne prenne pas en compte les nombres impairs
    // 2. Écrire une nouvelle fonction récursive qui permet de calculer n factoriel (exemple : 5 factoriel = 5 * 4 * 3 * 2 * 1). Pour simplifier, on part du principe que n sera toujours supérieur à 2.
  help: |-
    t[1:] signifie : on reprend t avec tous ses éléments excepté le premier element (t[0] est retiré)
  answers: 
    pseudocode: |-
      // 0 
      la fonction mystere(t) avec t=[4,9,0,5] exécutera l'addition suivante : 4 + 9 + 0 + 5 + 0, le résultat affiché sera alors 18

      // 1
      FONCTION mystere(t: tableau)
        SI(TAILLE(t) est égal à 0)
          RETOURNER 0
        SINON
          SI(t[0] est impair)
            RETOURNER mystere(t[1:])
          SINON
            RETOURNER t[0] + mystere(t[1:])
          FIN SI
        FIN SI
      FIN FONCTION


      // 2 
      FONCTION factorielle(n: entier)
        SI(n est égal à 1)
          RETOURNER 1
        SINON
          RETOURNER n * factorielle( n - 1 )
        FIN SI
      FIN FONCTION

    javascript: |-
      // 0 
      // la fonction mystere(t) avec t=[4,9,0,5] exécutera l'addition suivante : 4 + 9 + 0 + 5 + 0, le résultat affiché sera alors 18

      // 1
      fonction mystere(t)
      {
        if(t.length === 0) return 0;
        else 
        {
          if(t[0] % 2 !== 0)
          {
            return mystere(t.slice(1));
          }
          else
          {
            return t[0] + mystere(t.slice(1));
          }
        }
      }

      // 2 
      function factorielle(n) {
        if (n === 1) {
          return 1;
        } else {
          return n * factorielle(n - 1);
        }
      }

    php: |-
      // 0 
      // la fonction mystere(t) avec t=[4,9,0,5] exécutera l'addition suivante : 4 + 9 + 0 + 5 + 0, le résultat affiché sera alors 18

      // 1
      function mystere($t)
      {
          if (count($t) === 0) return 0;
          else {
              if ($t[0] % 2 !== 0) {
                  return mystere(array_slice($t, 1));
              } else {
                  return $t[0] + mystere(array_slice($t, 1));
              }
          }
      }

      // 2
      function factorielle($n)
      {
          if ($n === 1) {
              return 1;
          } else {
              return $n * factorielle($n - 1);
          }
      }

    python: |-
      # 0 
      # la fonction mystere(t) avec t=[4,9,0,5] exécutera l'addition suivante : 4 + 9 + 0 + 5 + 0, le résultat affiché sera alors 18

      # 1
      def mystere(t):
          if len(t) == 0:
              return 0
          else:
              if t[0] % 2 != 0:
                  return mystere(t[1:])
              else:
                  return t[0] + mystere(t[1:])

      # 2
      def factorielle(n):
          if n == 1:
              return 1
          else:
              return n * factorielle(n - 1)