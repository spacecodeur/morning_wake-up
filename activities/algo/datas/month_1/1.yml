-
  statement: |-
    Étant donné le pseudocode suivant : 
    
    i <- 0
    j <- TAILLE(mot) - 1
    TANT QUE i est inférieur à j
      SI mot[i] n'est pas égal à mot[j]
        RETOURNER FAUX
      FIN SI
      i <- i + 1
      j <- j - 1
    FIN TANT QUE
    RETOURNER VRAI

    0. Que retourne cet algorithme si la variable 'mot' a comme valeur 'été', 'maison' ou encore 'elle' ?
    1. Que semble permettre de faire cet algorithme ?
    2. Sortez votre ordinateur et implémentez cet algorithme : incluez le dans une fonction facilement ré-utilisable
  help: |-
    Questions 0 & 1 : essayez de dessiner étape par étape l'avancement de l'algorithme selon le mot donné
    Question 2 : le TANT QUE peut facilement être traduis avec un While en programmation
  answers:
    reponse_0: |-
      'été'   -> l'algorithme affichera VRAI
      'maison'-> l'algorithme affichera FAUX
      'elle'  -> l'algorithme affichera VRAI
    reponse_1: |-
      Cet algorithme permet de vérifier si un mot donné est un palindrome (mot qui se lit aussi bien de gauche à droite que de droite à gauche)
    reponse_3_javascript: |-
      function isPalindrome(mot)
      {
          let i = 0;
          let j = mot.length - 1;
          
          while(i < j)
          {
              if(mot[i] != mot[j])
              {
                  return false;
              }
              i = i + 1;
              j = j - 1;
          }
          return true;
      }
    reponse_3_php: |-
      function isPalindrome(string $mot): bool
      {
          $i = 0;
          $j = strlen($mot) - 1;
          while($i < $j)
          {
              if($mot[$i] != $mot[$j])
              {
                  return false;
              }
              $i = $i + 1;
              $j = $j - 1;
          }
          return true;
      }
    reponse_3_python: |-
      def isPalindrome(mot: str) -> bool:
          i = 0
          j = len(mot) - 1
          while i < j:
              if mot[i] != mot[j]:
                  return False
              i = i + 1
              j = j - 1
          return True

-
  statement: |-
    L'attaque par force brute (brut force) consiste à tester toutes les combinaisons possibles d'un mot de passe jusqu'à trouver le bon. 
    Supposons que nous avons à disposition la fonction `coffreFort(motDePasse)` : cette fonction returne `true` si motDePasse est valide, `false` sinon.
    Supposons, pour commencer, que `motDePasse` contient exactement deux chiffres ('00', '01', '02', ..., '99').
    0. Proposez un algorithe en pseudocode permettant de générer et stocker toutes les combinaisons de code possibles à 2 chiffres
    1. Améliorez votre algorithme afin de générer et stocker toutes les combinaisons de code possibles à 3 chiffres
    2. Sortez l'ordinateur et implémentez votre algorithme : testez toutes les combinaisons stockées avec la fonction `coffreFort` jusqu'à tomber sur le bon mot de passe
  answers:
    reponse_0: |-
      motsDePasse <- []
      POUR i ALLANT DE 0 À 9
        POUR j ALLANT DE 0 À 9
          motDePasse <- concaténation de i et de j
          AJOUTER motDePasse dans motsDePasse
        FIN POUR
      FIN POUR
    reponse_1: |-
      motsDePasse <- []
      POUR i ALLANT DE 0 À 9
        POUR j ALLANT DE 0 À 9
          POUR k ALLANT DE 0 À 9
            motDePasse <- concaténation de i, de j et de k
            AJOUTER motDePasse dans motsDePasse
          FIN POUR
        FIN POUR
      FIN POUR
    reponse_2_javascript: |-
      function generatePasswords(){
          let passwords = [];
          for(let i = 0 ; i < 10 ; i+=1)
          {
              for(let j = 0 ; j < 10 ; j+=1)
              {
                  for(let k = 0 ; k < 10 ; k+=1)
                  {
                      passwords.push(i.toString() + j.toString() + k.toString());
                  }
              }
          }
          return passwords;
      }

      function brutForce(passwords)
      {
          for(password of passwords)
          {
              if(coffreFort(password) === true)
              {
                  return password;
              }
          }
          return false;
      }

      passwords = generatePasswords();
      password = brutForce(passwords);
      console.log('Le mot de passe est : ' + password);
    reponse_2_php: |-
      function generatePasswords(): array
      {
        $passwords = [];
        for($i = 0 ; $i < 10 ; $i+=1)
        {
            for($j = 0 ; $j < 10 ; $j+=1)
            {
                for($k = 0 ; $k < 10 ; $k+=1)
                {
                    $passwords[] = $i . $j . $k;
                }
            }
        }
        return $passwords;
      }

      function brutForce(array $passwords): bool | string
      {
        foreach($passwords as $password)
        {
            if(coffreFort($password) === true)
            {
                return $password;
            }
        }
        return false;
      }

      $passwords = generatePasswords();
      $password = brutForce($passwords);
      echo 'Le mot de passe est : ' . $password;
    reponse_2_python: |-
      def generatePasswords() -> [str]:
          passwords = []
          for i in range(10):
              for j in range(10):
                  for k in range(10):
                      passwords.append(str(i) + str(j) + str(k))
          return passwords

      def brutForce(passwords) -> bool | str:
          for password in passwords:
              if coffreFort(password) == True:
                  return password
          return False

      passwords = generatePasswords()
      password = brutForce(passwords)
      print('Le mot de passe est : ' + password)
