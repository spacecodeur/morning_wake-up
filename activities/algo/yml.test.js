import { getAllYamlFilePaths, yamlFileToJavascriptObject } from "../../libraries/fileSystem.js";

const algoFilesPaths = getAllYamlFilePaths(__dirname);
// const algoFilesPaths = [__dirname + "\\datas\\dist.yml"]; // debug

let algoFileContent;
algoFilesPaths.forEach((algoFilePath)=>{
    algoFileContent = yamlFileToJavascriptObject(algoFilePath);
    
    // un fichier algo doit contenir un ensemble d'exercice
    describe('Check the algo file \'' + algoFilePath + '\'', () => {

        test('The algo file contains a set of exercises', ()=>{
            expect(algoFileContent.length).toBeGreaterThan(0);
        })

        algoFileContent.forEach((algoExercice, index)=>{

            test('The algo num \'' + index + '\' has a defined statement', ()=>{
                expect(algoExercice.statement).not.toEqual(undefined);
            });

            test('The algo num \'' + index + '\' has a statement who contains a non-empty text', ()=>{
                expect(algoExercice.statement).not.toEqual('');
            });

            // L'aide n'est pas forcément renseignée pour un algo
            if(algoExercice.hasOwnProperty('help'))
            {
                // si une aide est renseignée, on s'assure à ce qu'elle ne soit pas vide
                test('The algo num \'' + index + '\' contains a non-empty help', ()=>{
                    expect(algoExercice.help).not.toEqual('');
                });
            }

            // La clé 'answers' doit au moins avoir une réponse renseignée
            test('The algo num \'' + index + '\' contains a answer bloc', ()=>{
                expect(algoExercice.answers).not.toEqual(null);
            });

            if(null !== algoExercice.answers)
            {
                // La clé 'answers' doit au moins avoir une réponse renseignée
                test('The algo num \'' + index + '\' contains at last one answer', ()=>{
                    expect(Object.keys(algoExercice.answers).length).toBeGreaterThan(0);
                });

                for (let answer in algoExercice.answers) {
                    
                    // On vérifie si la réponse contient un texte
                    test('The answer \'' + answer + '\' in the algo num \'' + index + '\' contains a text', ()=>{
                        expect(algoExercice.answers[answer]).not.toEqual(null);
                    });

                    // On vérifie si le text contenu dans la réponse n'est pas vide
                    test('The answer \'' + answer + '\' in the algo num \'' + index + '\' contains a non-empty text', ()=>{
                        expect(algoExercice.answers[answer]).not.toEqual('');
                    });
                }
            }
        });
    });


});