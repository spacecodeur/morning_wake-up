"use strict";

import {
  askChoiceYesNo,
  title,
  titleWithTag,
  importantText,
  askChoice,
  askChoiceMultiple,
  askUnchoiceMultiple,
  pressEnterFor,
  error,
  input,
} from "../../libraries/terminal.js";
import {
  pickRandomLearner,
  loadLearnersObjectsFromFile,
  incrementScoreInLearnerConfigFile,
} from "../../libraries/learners.js";

const l = console.log;
const lc = console.clear;

async function run(oAlgos) {
  lc();

  let oLearners = loadLearnersObjectsFromFile();

  // On demande quels sont les apprenants présents
  let aLearnersPresents;
  do {
    aLearnersPresents = await askUnchoiceMultiple(
      "Tout le monde est bien présent ? (décocher les absents)",
      ...Object.keys(oLearners).map((name) => ({ value: name }))
    );
    if (aLearnersPresents.length === 0)
      l(error("Hmmm, il faut qu'il y ai au moins un présent !"));
  } while (aLearnersPresents.length === 0);

  // afficher tous les énoncés des algos
  oAlgos.forEach((oAlgo, index) => {
    l(title("Algo n°" + index));
    l(importantText(oAlgo.statement));
  });
  l(); // saut de ligne

  await pressEnterFor("show helps");
  lc();

  // proposer aides des algos
  oAlgos.forEach((oAlgo, index) => {
    l(titleWithTag("With-help(s)", "Algo n°" + index));
    l(importantText(oAlgo.statement) + "\n");
    if (oAlgo.help !== null) {
      l(oAlgo.help);
    } else {
      l("No help here !");
    }
  });
  l();

  // Correction
  await pressEnterFor("start the corrections");
  lc();

  for (let i = 0; i < oAlgos.length; i += 1) {
    l(titleWithTag("Correction", "Algo n°" + i));
    l(importantText(oAlgos[i].statement));
    l();

    let answer = await askChoice(
      "How is the corrector chosen ?",
      { name: "Randomly way", value: "random" },
      { name: "manually way", value: "manually" }
    );

    if (answer === "random") {
      let validated = false;
      let theChoosenOne;

      oLearners = await removeMissingsLearners(aLearnersPresents);

      do {
        theChoosenOne = pickRandomLearner(oLearners, "algo");
        l();
        l(importantText(theChoosenOne) + " ! are you ready ?!");
        l();

        validated = await askChoiceYesNo("confirm : ");
      } while (validated === "no");

      // on ajoute +1 dans le learners.yml à l'apprenant choisi
      // il aura moins de chance d'être choisi la fois prochaine
      incrementScoreInLearnerConfigFile(oLearners, theChoosenOne, "algo");
    }
    l();

    await pressEnterFor("show answer(s)");
    lc();

    let languagesChosen = [];
    let exit = false;
    do {
      if (languagesChosen.length !== 0) {
        await pressEnterFor("continue");
        lc();
      }

      languagesChosen = await askChoiceMultiple(
        "Which language ?",
        ...Object.keys(oAlgos[i].answers).map((language) => {
          return { value: language };
        }),
        { name: error("exit"), value: "exit" }
      );

      if (languagesChosen.includes("exit")) {
        if (languagesChosen.length === 1) {
          // seul exit a été sélectionné : on sort du do...while
          break;
        }
        exit = true;
        languagesChosen.splice(languagesChosen.indexOf("exit"), 1);
      }

      l(title("Algo n°" + i));
      l(importantText(oAlgos[i].statement));
      l();

      for (let j = 0; j < languagesChosen.length; j += 1) {
        l(titleWithTag(languagesChosen[j], "Algo n°" + i));
        l(oAlgos[i].answers[languagesChosen[j]]);
        l();
      }
    } while (exit === false);

    await pressEnterFor("next algo !");
    lc();
  }

  await input({ message: "no more Algo ! bye ! have a good day :D" });
}

async function removeMissingsLearners(aLearnersPresents) {
  // on reprend les datas dans le fichier .yml à chaque nouvelle question
  // pour avoir les dernières infos à jours pour l'algo ramdom
  let oLearners = await loadLearnersObjectsFromFile();

  // On vire les apprenants absents de la structure principale
  return Object.keys(oLearners)
    .filter((key) => aLearnersPresents.includes(key))
    .reduce((obj, key) => {
      obj[key] = oLearners[key];
      return obj;
    }, {});
}

async function showOrNotReminderPseudocode() {
  if (
    (await askChoiceYesNo("Show pseudocode cheatsheet (FR) ?", true)) === "yes"
  ) {
    l(`a <- 17
a <- a + 1

SI (expression)
ALORS
    // bloc de code 
FIN SI

RÉPÉTER 10 fois le bloc de code suivant
DÉBUT
    // bloc de code 
FIN RÉPÉTER

TANT QUE (expression)
DÉBUT
    // bloc de code 
FIN TANT QUE

POUR i ALLANT DE 0 à TAILLE(tab) - 1
DÉBUT
    // bloc de code
FIN POUR

FONCTION xxx (string paramètre1, integer paramètre2)
DÉBUT
    // bloc de code 
FIN FONCTION`);
  }
}

export { showOrNotReminderPseudocode, run };
