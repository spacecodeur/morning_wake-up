import { getAllYamlFilePaths, yamlFileToJavascriptObject } from "../../libraries/fileSystem.js";

const quizFilesPaths = getAllYamlFilePaths(__dirname);
// const quizFilesPaths = [__dirname + "\\datas\\dist.yml"]; // debug

let quizFileContent;
quizFilesPaths.forEach((quizFilePath)=>{
    quizFileContent = yamlFileToJavascriptObject(quizFilePath);

    // un fichier quiz doit contenir un ensemble de questions/réponses
    describe('Check the quiz file \'' + quizFilePath + '\'', () => {

        test('The quiz file contains a set of exercises', ()=>{
            expect(quizFileContent.length).toBeGreaterThan(0);
        })

        quizFileContent.forEach((quizExercice, index)=>{

            test('The quiz num \'' + index + '\' has a defined question', ()=>{
                expect(quizExercice.question).not.toEqual(undefined);
            });

            test('The quiz num \'' + index + '\' has a question who does not contains a non-empty text', ()=>{
                expect(quizExercice.question).not.toEqual('');
            });

            test('The quiz num \'' + index + '\' has a defined answer', ()=>{
                expect(quizExercice.answer).not.toEqual(undefined);
            });

            test('The quiz num \'' + index + '\' has a answer who does not contains a non-empty text', ()=>{
                expect(quizExercice.answer).not.toEqual('');
            });

        });
    });
});