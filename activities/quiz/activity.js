"use strict";

import {
  title,
  titleWithTag,
  importantText,
  askChoice,
  askChoiceYesNo,
  askUnchoiceMultiple,
  pressEnterFor,
  input,
} from "../../libraries/terminal.js";
import {
  pickRandomLearner,
  loadLearnersObjectsFromFile,
  incrementScoreInLearnerConfigFile,
} from "../../libraries/learners.js";

const l = console.log;
const lc = console.clear;

async function run(oQuiz) {
  lc();

  // mélange des questions (qu'importe leur ordre d'apparition)
  oQuiz = oQuiz.sort(() => Math.random() - 0.5);

  let oLearners = loadLearnersObjectsFromFile();

  // On demande quels sont les apprenants présents
  let aLearnersPresents;
  do {
    aLearnersPresents = await askUnchoiceMultiple(
      "Tout le monde est bien présent ? (décocher les absents)",
      ...Object.keys(oLearners).map((name) => ({ value: name }))
    );
    if (aLearnersPresents.length === 0)
      l(error("Hmmm, il faut qu'il y ai au moins un présent !"));
  } while (aLearnersPresents.length === 0);

  for (let i = 0; i < oQuiz.length; i += 1) {
    let answer = await askChoice(
      "How is the corrector chosen ?",
      { name: "Randomly way", value: "random" },
      { name: "manually way", value: "manually" }
    );

    if (answer === "random") {
      let validated = false;
      let theChoosenOne;

      oLearners = await removeMissingsLearners(aLearnersPresents);

      do {
        theChoosenOne = pickRandomLearner(oLearners, "quiz");
        l();
        l(importantText(theChoosenOne) + " ! are you ready ?!");
        l();

        validated = await askChoiceYesNo("confirm : ");
      } while (validated === "no");

      // on ajoute +1 dans le learners.yml à l'apprenant choisi
      // il aura moins de chance d'être choisi la fois prochaine
      incrementScoreInLearnerConfigFile(oLearners, theChoosenOne, "quiz");
    }

    lc();

    l(title("Question " + i + "/" + (oQuiz.length - 1).toString()));
    l(importantText(oQuiz[i].question));
    l();

    await pressEnterFor("show answer");

    l(
      titleWithTag(
        "Answer",
        "Question " + i + "/" + (oQuiz.length - 1).toString()
      )
    );
    l(importantText(oQuiz[i].answer));
    l();

    await pressEnterFor("next question !");
    lc();
  }

  await input({ message: "no more Question ! bye ! have a good day :D" });
}

async function removeMissingsLearners(aLearnersPresents) {
  // on reprend les datas dans le fichier .yml à chaque nouvelle question
  // pour avoir les dernières infos à jours pour l'algo ramdom
  let oLearners = await loadLearnersObjectsFromFile();

  // On vire les apprenants absents de la structure principale
  return Object.keys(oLearners)
    .filter((key) => aLearnersPresents.includes(key))
    .reduce((obj, key) => {
      obj[key] = oLearners[key];
      return obj;
    }, {});
}

export { run };
