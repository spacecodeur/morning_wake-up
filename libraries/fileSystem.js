'use strict';

import fs from 'fs';
import path from 'path';
import YAML from 'yaml';

function getAvailablesDifficulties(sRessource)
{
    let files = fs.readdirSync('./activities/' + sRessource + '/datas', { withFileTypes: true });

    return files
        .filter((item) => item.isDirectory())
        .map((item) => item.name);
}

function getAvailablesActivities()
{
    let files = fs.readdirSync('./activities', { withFileTypes: true });

    return files
        .filter((item) => item.isDirectory())
        .map((item) => item.name);
}

function yamlFileToJavascriptObject(pathFile)
{
    const fRessource = fs.readFileSync(pathFile, 'utf8');
    return YAML.parse(fRessource);
}

function updateYamlFileWithJavascriptObject(pathYmlFile, oObject)
{
    const yamlContent = YAML.stringify(oObject);

    fs.writeFileSync(pathYmlFile, yamlContent, 'utf8');
}

function isFileExist(pathFile)
{
    return fs.existsSync(pathFile);
}

function isDirExists(pathDir)
{
  return fs.existsSync(pathDir) && fs.lstatSync(pathDir).isDirectory();
}

//  utilisé pour les tests ('tention, fonction recursive)
function getAllYamlFilePaths(directory)
{
    const yamlFilePaths = [];

    function traverse(dir) {
      const files = fs.readdirSync(dir);
  
      for (const file of files) {
        const filePath = path.join(dir, file);
        const stat = fs.statSync(filePath);
  
        if (stat.isDirectory()) {
          traverse(filePath);
        } else if (path.extname(file) === '.yaml' || path.extname(file) === '.yml') {
          // Check if the file has a .yaml or .yml extension
          yamlFilePaths.push(filePath);
        }
      }
    }
  
    traverse(directory);
    return yamlFilePaths;    
}

export
{
    yamlFileToJavascriptObject,
    updateYamlFileWithJavascriptObject,
    getAvailablesDifficulties,
    getAvailablesActivities,
    isFileExist,
    isDirExists,
    getAllYamlFilePaths
};
