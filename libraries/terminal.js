"use strict";

import { select, checkbox, confirm as conf, input } from "@inquirer/prompts";
import chalk from "chalk";

const title_primary = chalk.bgBlue.bold.white;
const tag = chalk.bgGreen.bold.white;
const important = chalk.bold.black;
const err = chalk.bold.red;

function title(sTitle) {
  return "\n" + title_primary(" " + sTitle + " ") + "\n";
}

function titleWithTag(sTag, sTitle) {
  return (
    "\n" +
    tag(" [#" + sTag + "] ") +
    " " +
    title_primary(" " + sTitle + " ") +
    "\n"
  );
}

function importantText(sText) {
  return important(sText);
}

function error(sText) {
  return err(sText);
}

async function askChoice(sMessage, ...oChoices) {
  return await select({
    message: sMessage,
    choices: oChoices,
  });
}

async function askChoiceMultiple(sMessage, ...oChoices) {
  return await checkbox({
    message: sMessage,
    choices: oChoices,
  });
}

async function askUnchoiceMultiple(sMessage, ...oChoices) {
  const updatedChoices = oChoices.map((choice) => ({
    ...choice,
    checked: true,
  }));

  return await askChoiceMultiple(sMessage, ...updatedChoices);
}

async function askChoiceYesNo(sMessage, noIsDefaultAnswer = false) {
  let choices = [{ value: "yes" }, { value: "no" }];
  if (noIsDefaultAnswer) {
    choices = choices.reverse();
  }
  return await askChoice(sMessage, ...choices);
}

async function confirm(sMessage) {
  return await conf({ message: sMessage });
}

async function pressEnterFor(sMessage) {
  await input({ message: "Press [enter] for " + sMessage });
}

export {
  askChoice,
  askChoiceMultiple,
  askUnchoiceMultiple,
  askChoiceYesNo,
  title,
  titleWithTag,
  importantText,
  confirm,
  error,
  input,
  pressEnterFor,
};
