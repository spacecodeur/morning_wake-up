"use strict";

import {
  updateYamlFileWithJavascriptObject,
  yamlFileToJavascriptObject,
} from "./fileSystem.js";

function pickRandomLearner(oLearners, sActivity) {
  let keys = Object.keys(oLearners);
  let min = oLearners[keys[0]][sActivity];
  let aMinLearners = [];

  // on détermine la valeur min
  for (let i = 1; i < keys.length; i += 1) {
    if (min > oLearners[keys[i]][sActivity]) {
      min = oLearners[keys[i]][sActivity];
    }
  }

  // on mets tous les apprenants qui ont cette valeur min dans une liste a part
  for (let i = 0; i < keys.length; i += 1) {
    if (oLearners[keys[i]][sActivity] === min) {
      aMinLearners.push(keys[i]);
    }
  }

  // on retourne un apprenant random depuis la nouvelle liste
  return aMinLearners[Math.floor(Math.random() * aMinLearners.length)];
}

function incrementScoreInLearnerConfigFile(oLearners, sLearner, sActivity) {
  let currentScore = oLearners[sLearner][sActivity];
  oLearners[sLearner][sActivity] = currentScore + 1;

  let currentOLearners = loadLearnersObjectsFromFile();

  for (const [key, value] of Object.entries(oLearners)) {
    currentOLearners[key] = value;
  }

  updateYamlFileWithJavascriptObject("./learners.yml", currentOLearners);
}

function loadLearnersObjectsFromFile() {
  return yamlFileToJavascriptObject("./learners.yml");
}

export {
  pickRandomLearner,
  incrementScoreInLearnerConfigFile,
  loadLearnersObjectsFromFile,
};
