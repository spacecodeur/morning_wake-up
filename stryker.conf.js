/**
* @type {import('@stryker-mutator/api/core').StrykerOptions}
*/
module.exports = {
    mutate: [
      '!**/*.js',
      'activities/**/activity.js',
      'libraries/**/*.js'
    ],
    testRunner: 'jest',
    jest: {
        "projectType": "custom",
        "enableFindRelatedTests": true
    },
    reporters: ['progress', 'clear-text', 'html'],
    coverageAnalysis: 'perTest'
  };