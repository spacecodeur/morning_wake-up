# Welcome to the Morning wake-up !

Morningwakepup est une petite application [CLI](https://en.wikipedia.org/wiki/Command-line_interface) permettant de lancer des sessions d'exercices dans le cadre de formations type DWWM, CDA, Data, ...

Actuellement deux types d'exercices sont disponibles : 
- les exercices de type : **algo**
- les exercices de type : **quiz**

## Installation

- Installer [nodejs](https://nodejs.org/en/download/current) sur son ordi 
- Cloner ce projet 
- Se rendre dans le dossier du projet à l'aide de son terminal préféré
- Exécuter `npm install`

Plusieurs commandes sont disponibles, n'hésitez pas à jeter un oeil dans le package.json. Les trois commandes principales sont : 

- `npm start` : démarre une session d'exercice avec vos apprenants
    - le terminal demandera un `id` d'activité, l'`id` correspond au numéro du fichier contenant le/les exercice(s) qui se trouve dans un des sous-dossiers de `activites/{algo|quiz}/datas`
- `npm test:yml` : test si tous les contenus des activités, écrites en yaml, ne présentent pas d'anomalie. Pratique pour vérifier si on a bien écrit ses exercices 
- `npm run jshint` : analyse statique du code source, utile avant un `git push` si vous voulez contribuer au projet !

## Configuration

Les apprenants de la promos sont à renseigner dans un fichier nommé `learners.yml` à placer à la racine du projet.

Un fichier 'squelette' nommé `learners.yml.dist` est par défaut présent. Il suffit alors de le renommer en `learners.yml` et d'y renseigner dedans les prénoms des apprenants.

## Création d'exercice 

Les exercices d'algos et quiz se trouvent respectivement dans `activities/algo/datas` et `activities/quiz/datas`. Dans ces deux dossiers se trouvent un fichier 'squelette' nommé `dist.yml`.

Il suffit de copier ce fichier dans un des sous-dossiers dans `datas` et de le renommer en `{id}.yml` où `{id}` correspond à un numéro unique dans la liste d'exerices du dossier `datas` (`activites/{algo|quiz}/datas`).

## Pas/peu de commentaires ? pas de tests automatiques ? 

Cette absence résulte d'un subtile mélange entre flemme et pédagogie, les explications : 
- la flemme bien sur
- l'application n'est pas très complexe et n'a pas vocation à faire plus que ce qu'elle ne fait aujourd'hui en terme de fonctionnalité
- la flemme encore un peu
- l'aspect pédagogique pour les éventuels apprenants qui passent par ici :
    - pour ne pas trop macher le travail sur l'analyse et la compréhension interne du programme : l'analyse peut s'appuyer sur les noms des variables et fonctions choisis avec soin
    - le programme utilise différents algorithmes 'métiers' intéressant à analyser pour en percer les mystères :
        - comment les apprenants sont choisis aléatoirement ?
        - comment les données sur les apprenants sont sauvegardées ?
        - etc...

En outre, ce projet peut faire office de bac à sable pour d'éventuelles activités introductives sur les tests autos. (surtout les tests unitaires)

/!\ une petite indication tout de même pour savoir par où commencer si vous souhaitez analyser/explorer le code : la commande `npm start` exécute le fichier JS `main.js` /!\
